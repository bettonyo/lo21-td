#ifndef SIMULATEUR_H_INCLUDED
#define SIMULATEUR_H_INCLUDED

#include "automate.h"
using namespace std;

class Simulateur {
private:
    const Automate& automate;
    const Etat* depart; // pointe sur un eventuel �tat de d�part (s'il a �t� donn�)
    size_t nbMaxEtats;
    Etat** etats; //pointe sur un tableau de Etat*, allou� dynamiquement, de taille nbMaxEtats
    //initiallement, tous les pointeurs du tableau etats sont init � nullptr
    //etat[0] pointe sur une copie de l'etat de d�part
    /// comme on a des pointeurs (de pointeurs), il faudra redefnir le constructeur de recopie et l'op�rateur d'affectation : fait plus tard dans un autre TD
    /// comme on a des donn�es allou�es dynamiquement, il faudra un destructeur pour desallouer la m�moire
    size_t rang; // permet de sauvegarder le rang du dernier �tat g�n�r�
    // le rang n'est pas remis � z�ro quand on arrive dans la situation o un �tat es stock� au d�but du buffer

public:
    Simulateur(const Automate& a, size_t buf = 2);
    Simulateur(const Automate& a, const Etat& dep, size_t buf = 2);

    size_t getRangDernier() const { return rang; }
    const Etat& dernier() const;

    void setEtatDepart(const Etat& e); // rang = 0

    void next();
    void run(size_t nbSteps);
    void reset();

    /// IL FAUT UN DESTRUCTEUR !! !! !! ! et aussi red�finir le constructeur par recopie et l'operateur d'affectation.
    ~Simulateur();

    /// Impl�mentation du DP Iterator pour parcourir s�quentiellement les Etats stock�s
    class Iterator {
    private:

        Simulateur* sim;
        int iCurrent;
        int nb;

        friend class Simulateur;

        /// constructeurs
        Iterator() : sim(nullptr), iCurrent(0), nb(0) {}
        Iterator(Simulateur* s) : sim(s), iCurrent(s->rang % s->nbMaxEtats), nb(0) {}

    public:
        Etat& current() const;
        void next();
        bool isDone() const ;

        /// am�lioration de l'interface :
        Iterator (const Iterator&) = default;

    };
    /// m�thode first() : en dehors de la classe Iterator : voila pourquoi on a besoin de la d�claration d'amiti� : cette m�thod utilise le constructeur Iterator()
    Iterator getIterator();
    friend class Simulateur::Iterator;

    /// Implementation du const iterator : it�rateur en lecture !
    class ConstIterator {
    private:

        const Simulateur* sim;
        int iCurrent;
        int nb;

        friend class Simulateur;

        /// constructeurs
        ConstIterator() : sim(nullptr), iCurrent(0), nb(0) {}
        ConstIterator(const Simulateur* s) : sim(s), iCurrent(s->rang % s->nbMaxEtats), nb(0) {}

    public:
        const Etat& current() const;
        void next();
        bool isDone() const ;

        /// am�lioration de l'interface :
        ConstIterator (const ConstIterator&) = default;

    };
    /// m�thode first() : en dehors de la classe Iterator : voila pourquoi on a besoin de la d�claration d'amiti� : cette m�thod utilise le constructeur Iterator()
    ConstIterator getConstIterator();
    friend class Simulateur::ConstIterator;


    /// Implementation de la version standards de C++(STL)
    class iterator {
    private:
        Simulateur* sim;
        int iCurrent;
        int nb;
        friend class Simulateur;

        iterator() : sim(nullptr), iCurrent(0), nb(0) {}
        iterator(Simulateur* s) : sim(s), iCurrent(s->rang % s->nbMaxEtats), nb(0) {}

    public:
        bool isDone() const ;
        Etat& operator*() const; /// remplace la m�thode current()
        bool operator!=(iterator it);/// compl�te la m�thode isDone()
        void operator++(); /// remplace la m�thode next()

    };
    /// remplace la m�thode getIterator()
    iterator begin();
    iterator end();

/// Implementation (en lecture) de la version standards de C++(STL)
    class const_iterator {
    private:
        const Simulateur* sim;
        int iCurrent;
        int nb;
        friend class Simulateur;

        const_iterator() : sim(nullptr), iCurrent(0), nb(0) {}
        const_iterator(const Simulateur* s) : sim(s), iCurrent(s->rang % s->nbMaxEtats), nb(0) {}

    public:
        bool isDone() const ;
        const Etat& operator*() const; /// remplace la m�thode current()
        bool operator!=(const_iterator it);/// compl�te la m�thode isDone()
        void operator++(); /// remplace la m�thode next()

    };
    /// remplace la m�thode getConstIterator()
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;


};


#endif // SIMULATEUR_H_INCLUDED
