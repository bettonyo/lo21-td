#ifndef ETAT_H_INCLUDED
#define ETAT_H_INCLUDED

#include <iostream>
#include <cstddef>
using namespace std;

class Etat { /// possibilit�s de dupliquer par construction et par affectation ==> red�fintion du constructeur de recopie et de l'op�rateur d'affectation
private:
    size_t dimension = 0;
    bool* valeur; //pointe sur un tableau de taille dimension allou� dynamiquement
public:
    /// constructeur
    Etat (size_t dim);
    /// constructeur de recopie : important car tableau dynamique donc il ne suffit pas de copier l'adresse stocker dans le pointeur !
    Etat (const Etat& e);
    /// accesseurs en lecture
    size_t getDimension() const { return dimension; }
    bool getCellule(size_t i) const;
    /// accesseurs en �criture
    void setCellule(size_t i, bool val);
    /// surcharge operateur operator=
    Etat& operator=(const Etat& e);
    /// destructeur
    ~Etat();
};

/// surcharge operateur operator<< : doit �tre faite en dehors de la classe !
ostream& operator<<(ostream& f, const Etat& e);

#endif // ETAT_H_INCLUDED
