#include "automate.h"

/// 2 constructeurs : on utilise les 2 fonction HELPERS
Automate::Automate (unsigned short num) : numero(num), numeroBit (NumToNumBit(num)) {}
Automate::Automate (const string& numBit) : numero(NumBitToNum(numBit)), numeroBit (numBit) {}

/// Transition : permet de passer d'un �tat � l'�tat suivant en appliquant la r�gle d�finie dans l'automate

void Automate::appliquerTransition (const Etat& dep, Etat& dest) const {

    if (dep.getDimension() != dest.getDimension()) {
        throw AutomateException("Diff�rence de dimensions entre dep et dest");
    }

    for (size_t i=0; i<dep.getDimension(); i++) {

        int decimal = 0;
        // partie gauche
        if (i > 0) decimal += dep.getCellule(i-1)*4;
        // partie centrale
        decimal += dep.getCellule(i)*2;
        // partie droite
        if (i < dep.getDimension()-1) decimal += dep.getCellule(i+1);

        char sortie = numeroBit[7 - decimal]; // 7 ici car il n'y a que 8 combinaisons possibles 2**3 = 8

        dest.setCellule(i, sortie - '0'); // - '0' : transforme le caract�re en int (code ascii)
    }

}

/// surcharge operator<<
ostream& operator<<(ostream& f, const Automate& a) {
    f << a.getNumero() << " : " << a.getNumeroBit() << std::endl;
    return f;
}

/// HELPERS : fonctions fournies dans l'�nonc� :

unsigned short NumBitToNum(const std::string& num) {
    if (num.size() != 8) throw AutomateException("Numero d�automate indefini");
    int puissance = 1;
    unsigned short numero = 0;
    for (int i = 7; i >= 0; i--) {
        if (num[i] == '1') numero += puissance;
        else if (num[i] != '0') throw AutomateException("Numero d�automate indefini");
        puissance *= 2;
    }
    return numero;
}

std::string NumToNumBit(unsigned short num) {
    std::string numeroBit;
    if (num > 256) throw AutomateException("Numero d�automate indefini");
    unsigned short p = 128;
    int i = 7;
    while (i >= 0) {
        if (num >= p) { numeroBit.push_back('1'); num -= p; }
        else { numeroBit.push_back('0'); }
        i--;
        p = p / 2;
    }
    return numeroBit;
}

