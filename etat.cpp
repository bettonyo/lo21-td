#include "etat.h"
#include "automate.h"

/// constructeur
Etat::Etat (size_t dim) : dimension(dim), valeur(nullptr) {
    if (dim != 0) {
        valeur = new bool[dim] {false};
        //for (size_t i=0; i<dim; i++)
            //valeur[i] = false;
    }
}

/// constructeur de recopie
Etat::Etat (const Etat& e) : dimension(e.dimension), valeur(nullptr) {
    if (e.dimension != 0) {
        valeur = new bool[e.dimension];
        for (size_t i=0; i<dimension; i++)
            valeur[i] = e.valeur[i];
    }
}

/// accesseur en lecture
bool Etat::getCellule(size_t i) const {
    if (i >= dimension)
        throw AutomateException("i non respect de la dimension");
    return valeur[i];
}

/// accesseur en �criture
void Etat::setCellule(size_t i, bool val) {
    if (i >= dimension)
        throw AutomateException("i : non respect de la dimension");
    valeur[i] = val;
}

/// surcharge operator<<
/*
ostream& operator<<(ostream& f, const Etat& e) {
    for (size_t i=0; i<e.getDimension(); i++) {
        f << e.getCellule(i);
    }
    return f;
}
*/

ostream& operator<<(ostream& f, const Etat& e) {
    for (size_t i=0; i<e.getDimension(); i++) {
        if (e.getCellule(i) == false)
            f << " ";
        else
            f << "X";
    }
    return f;
}


/// surcharge operator=
Etat& Etat::operator=(const Etat& e) {
    if (this != &e) { // ne pas affecter un objet � lui m�me
        dimension = e.dimension;
        delete[] valeur; //ou ~Etat() ?
        valeur = nullptr;
        if (e.valeur != nullptr and e.dimension != 0) {
            valeur = new bool[e.dimension];
            for (size_t i=0; i<dimension; i++)
                valeur[i] = e.valeur[i];
        }
    }
    return *this; // car on veut renvoyer une r�f�rence : il n'y a donc pas de retour par recopie

}

/// destructeur
Etat::~Etat() {
    delete[] valeur;
}
