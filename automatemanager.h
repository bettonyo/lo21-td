#ifndef AUTOMATEMANAGER_H_INCLUDED
#define AUTOMATEMANAGER_H_INCLUDED

#include "automate.h"

/// impl�mentation du DesignPattern Singleton pour la classe AutomateManager : 1ere impl�mentation du cours
/// classe responsable de la gestion des Automates : cr�ation, destruction et sauvegarde

class AutomateManager{
private:
    Automate* automates[256]; /// tableau permettant de stocker les automates

    /// seul l'instance de la classe permet de cr�er des objets Automates : les constructeurs doivent �tre priv�s
    AutomateManager();
    ~AutomateManager();
    /// on veut �viter la duplication :
    AutomateManager(const AutomateManager&) = delete;
    AutomateManager& operator= (const AutomateManager& am) = delete;

    static AutomateManager* instance; ///unique instance du singleton

public:
    const Automate& getAutomate(unsigned short num);
    const Automate& getAutomate(const string& numBit); /// const ou pas const string& ?

    /// m�thode static accessible sans objets ! Elles donnent acc�s qu'aux attributs static
    static AutomateManager& donneAutomateManager();
    static void libererAutomateManager();

};
#endif // AUTOMATEMANAGER_H_INCLUDED
