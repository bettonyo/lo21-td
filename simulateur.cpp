#include "simulateur.h"


/// constructeurs : l'�tat de d�part sera pr�cis� plus tard en utilisant setEtatDepart()
Simulateur::Simulateur(const Automate& a, size_t buf) : automate(a), nbMaxEtats(buf), depart(nullptr), etats(nullptr), rang(0){
    if (nbMaxEtats > 0) {
        etats = new Etat*[nbMaxEtats] {nullptr};
        /*for (size_t i=0;i<nbMaxEtats;i++)
            etats[i] = nullptr;*/
    }
}

/// constructeurs : �tat de d�part pr�cis�
Simulateur::Simulateur(const Automate& a, const Etat& dep, size_t buf) : automate(a), nbMaxEtats(buf), depart(&dep), etats(nullptr), rang(0){
    if (nbMaxEtats > 0) {
        etats = new Etat*[nbMaxEtats] {nullptr}; // la m�moire de etats est allou�e. Mais ce sont des pointeurs qui sont stock�s dedans : la m�moire de ce sur quoi pointent les pointeurs n'est pas encore allou�e !
        etats[0] = new Etat(dep); // construction par recopie de dep
    }
}

/// g�n�rer l'�tat de la g�n�ration rang+1 : se servir de appliquerTransition
void Simulateur::next() {
    if (depart == nullptr)
        throw AutomateException("etat de depart non defini");

    size_t iDepart = rang % nbMaxEtats;
    rang++;
    size_t iDest = rang % nbMaxEtats;

    if (etats[iDest] == nullptr) etats[iDest] = new Etat(etats[iDepart]->getDimension());

    automate.appliquerTransition(*etats[iDepart], *etats[iDest]);
    /// *etats[iDepart] et *etats[iDest] sont bien des lvalues donc on peut les utiliser directement : prototype des param�tre de appliquerTransition : r�f�rence
    /* //En developpant :
    const Etat& dep = *etats[iDepart];
    Etat& dest = *etats[iDest];
    automate.appliquerTransition(dep, dest);
    *etats[iDepart] = dep;
    *etats[iDest] = dest;
    */

 }

void Simulateur::run (size_t nbSteps) {
    for (size_t i=0; i<nbSteps; i++)
        next();
 }

const Etat& Simulateur::dernier() const {
    if (depart == nullptr)
        throw AutomateException("etat de depart non defini");
    return *(etats[rang % nbMaxEtats]);
 }

void Simulateur::reset () {
    if (depart != nullptr) {
        if (etats[0] == nullptr) {
                etats[0] = new Etat(*depart); // construction par recopie
        }
        else {
            *etats[0] = *depart; //affectation
        }
        rang = 0;
    }
    else {
        throw AutomateException("Etat de depart non defini");
    }
}

/// accesseurs en �criture
void Simulateur::setEtatDepart(const Etat& e){
    depart = &e;
    reset();
}

Simulateur::~Simulateur() {
    for (size_t i=0; i<nbMaxEtats;i++) {
        delete etats[i]; // le destructeur ~Etat sera appel� pour chaque tableau valeur
    }
    delete[] etats;
}

/// ITERATOR et ses m�thodes :

Etat& Simulateur::Iterator::current() const {
    if (isDone())
        throw AutomateException("Iterator is done");

    return *(sim->etats[iCurrent]);
}

void Simulateur::Iterator::next() {

    if (isDone())
        throw AutomateException("Iterator is done");

    iCurrent--;
    nb++;
    /// Si iCurrent arrive � -1 et qu'il y a tout de m�me d'autres �tats
    /// � parcourir � la fin du tableau etats, alors on se place � la derni�re case.
    /// Sinon, on reste � -1, ce qui veut dire qu'on a fini
    if (iCurrent==-1 && sim->rang >= sim->nbMaxEtats)
        iCurrent = sim->nbMaxEtats - 1;

}

bool Simulateur::Iterator::isDone() const {
    return (sim==nullptr || iCurrent == -1 || nb == sim->nbMaxEtats);
}

Simulateur::Iterator Simulateur::getIterator() {
    return Simulateur::Iterator(this);
}

/// Iterateur en lecture :

const Etat& Simulateur::ConstIterator::current() const {
    if (isDone())
        throw AutomateException("Iterator is done");

    return *(sim->etats[iCurrent]);
}

void Simulateur::ConstIterator::next() {

    if (isDone())
        throw AutomateException("Iterator is done");

    iCurrent--;
    nb++;
    /// Si iCurrent arrive � -1 et qu'il y a tout de m�me d'autres �tats
    /// � parcourir � la fin du tableau etats, alors on se place � la derni�re case.
    /// Sinon, on reste � -1, ce qui veut dire qu'on a fini
    if (iCurrent==-1 && sim->rang >= sim->nbMaxEtats)
        iCurrent = sim->nbMaxEtats - 1;

}

bool Simulateur::ConstIterator::isDone() const {
    return (sim==nullptr || iCurrent == -1 || nb == sim->nbMaxEtats);
}

Simulateur::ConstIterator Simulateur::getConstIterator() {
    return Simulateur::ConstIterator(this);
}


/// It�rateur en �criture de la version standard C++(STL)

bool Simulateur::iterator::isDone() const {
    return (sim==nullptr || iCurrent == -1 || nb == sim->nbMaxEtats);
}

// remplace la m�thode current()
Etat& Simulateur::iterator::operator*() const{
    if (isDone())
        throw AutomateException("Iterator is done");

    return *(sim->etats[iCurrent]);
}

// compl�te la m�thode isDone()
bool Simulateur::iterator::operator!=(iterator it){
    //deux it�rateurs �gaux s'ils d�signent le m�me �l�ment :
    return iCurrent != it.iCurrent;
}

// remplace la m�thode next()
void Simulateur::iterator::operator++() {
    if (isDone())
        throw AutomateException("Iterator is done");

    iCurrent--;
    nb++;
    // Si iCurrent arrive � -1 et qu'il y a tout de m�me d'autres �tats
    // � parcourir � la fin du tableau etats, alors on se place � la derni�re case.
    // Sinon, on reste � -1, ce qui veut dire qu'on a fini
    if (iCurrent==-1 && sim->rang >= sim->nbMaxEtats)
        iCurrent = sim->nbMaxEtats - 1;

}

Simulateur::iterator Simulateur::begin(){
    return Simulateur::iterator(this);
}

Simulateur::iterator Simulateur::end(){
    /// il faut construire l'it�rateur FINI : il faut mettre � jour it.iCurrent
    iterator it(this); //construction par recopie.
    if (rang < nbMaxEtats)
        it.iCurrent = -1;
    else
        it.iCurrent = rang - nbMaxEtats;
    return it;

}

/// It�rateur en lecture de la version standard C++(STL)

bool Simulateur::const_iterator::isDone() const {
    return (sim==nullptr || iCurrent == -1 || nb == sim->nbMaxEtats);
}


// remplace la m�thode current()
const Etat& Simulateur::const_iterator::operator*() const{
    if (isDone())
        throw AutomateException("Iterator is done");

    return *(sim->etats[iCurrent]);
}

// compl�te la m�thode isDone()
bool Simulateur::const_iterator::operator!=(const_iterator it){
    //deux it�rateurs �gaux s'ils d�signent le m�me �l�ment :
    return iCurrent != it.iCurrent;
}

// remplace la m�thode next()
void Simulateur::const_iterator::operator++() {
    if (isDone())
        throw AutomateException("Iterator is done");

    iCurrent--;
    nb++;
    // Si iCurrent arrive � -1 et qu'il y a tout de m�me d'autres �tats
    // � parcourir � la fin du tableau etats, alors on se place � la derni�re case.
    // Sinon, on reste � -1, ce qui veut dire qu'on a fini
    if (iCurrent==-1 && sim->rang >= sim->nbMaxEtats)
        iCurrent = sim->nbMaxEtats - 1;

}

Simulateur::const_iterator Simulateur::begin() const {
    return Simulateur::const_iterator(this);
}

Simulateur::const_iterator Simulateur::end() const {
    /// il faut construire l'it�rateur FINI : il faut mettre � jour it.iCurrent
    const_iterator it(this); //construction par recopie.
    if (rang < nbMaxEtats)
        it.iCurrent = -1;
    else
        it.iCurrent = rang - nbMaxEtats;
    return it;
}

Simulateur::const_iterator Simulateur::cbegin() const {
    return begin();
}

Simulateur::const_iterator Simulateur::cend()const {
    return end();
}
