QT += widgets

SOURCES += \
    autocell.cpp \
    main.cpp \
    automate.cpp \
    etat.cpp \
    simulateur.cpp \
    automatemanager.cpp

HEADERS += \
    autocell.h \
    automate.h \
    etat.h \
    simulateur.h \
    automatemanager.h
