#include "automatemanager.h"
#include "automate.h"

///attribut static � d�clarer dans le cpp
AutomateManager* AutomateManager::instance = nullptr;

AutomateManager::AutomateManager() {
    for (size_t i =0; i<256; i++)
        automates[i] = nullptr;
}

AutomateManager::~AutomateManager() {
    for (size_t i =0; i<256; i++)
        if (automates[i] != nullptr)
            delete automates[i];
}

const Automate& AutomateManager::getAutomate(unsigned short num){
    /// si l'automate deand� n'existe pas, on le cr�e
    if (automates[num]==nullptr)
        automates[num] = new Automate(num); /// possible car d�claration d'amiti� faite dans la d�finition de la classe Automate
    return *automates[num];
}

const Automate& AutomateManager::getAutomate(const string& numBit) {
    return getAutomate(NumBitToNum(numBit));
}

AutomateManager& AutomateManager::donneAutomateManager(){
    if (instance == nullptr)
        instance = new AutomateManager; /// constructeur sans argument (d�clar� dans la partie priv�e ici)
    return *instance;
}

void AutomateManager::libererAutomateManager(){
    delete instance;
    instance = nullptr; /// bonne pratique
}
