#ifndef AUTOMATE_H_INCLUDED
#define AUTOMATE_H_INCLUDED

#include <string>
#include "etat.h"
using namespace std;

class AutomateException {
public:
    AutomateException(const std::string& message):info(message) {}
    std::string getInfo() const { return info; }
private:
    std::string info;
};

class Automate {
private:
    unsigned short numero;
    string numeroBit;
    /// PASSAGE DES CONSTRUCTEURS dans la partie priv�e pour emp�cher l'utilisateur de cr�er lui m�me des automates :
    Automate (unsigned short num);
    Automate (const string& numBit);
    /// on d�sactive aussi le constructeur par recopie et l'op�rateur d'affectation de la partie publique :
    Automate (const Automate&) = default;
    Automate& operator= (const Automate&) = default;
    /// d�claration d'amiti� pour que les objets AutomateManager puissent utiliser le constructeur Automate car il est � pr�sent priv�
    friend class AutomateManager;

public:
    /// accesseurs en lecture
    unsigned short getNumero() const { return numero;}
    string getNumeroBit() const { return numeroBit;}

    /// on desactive le constructeur par d�faut en public
    Automate() = delete;

    void appliquerTransition (const Etat& dep, Etat& dest) const;

    //~Automate() = default;
    /// il n'y a pas besoin de redefinir un destructeur, ni de surcharger l'op�rateur d'affectation par d�faut ni de red�finir le constructeur de recopie car ils sont suffiants
};

/// surcharge de operator<<
ostream& operator<<(ostream& f, const Automate& a);

/// HELPERS :
unsigned short NumBitToNum(const std::string& num);
std::string NumToNumBit(unsigned short num);


#endif // AUTOMATE_H_INCLUDED
