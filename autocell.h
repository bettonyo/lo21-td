#ifndef AUTOCELL_H
#define AUTOCELL_H

#include <QWidget>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QIntValidator>
#include <QString>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTableWidget>
#include <QPushButton>
//#include <QTableView> QTableView est remplacé par QTableWidget

class AutoCell : public QWidget {
    Q_OBJECT

    QSpinBox* num; // numéro de l'automate
    QLineEdit* numeroBit[8]; // un QLineEdit par bit
    QLabel* numl;
    QLabel* numeroBitl[8];

    QVBoxLayout* numc;
    QVBoxLayout* bitc[8];
    QHBoxLayout* numeroc;
    QVBoxLayout* mainLayout; // ajouter : mailLayout vertical

    QIntValidator* zeroOneValidator;

    QTableWidget* depart; // permet de représenter l'état de départ

    QPushButton* simulation; // permet de lancer la simulation

    QTableWidget* etats; // pour gérer les états générés par la simulation

    unsigned int dimension = 25; // dimension de l'automate généré
    unsigned int taille = 25; // taille des cellules

    // pb : le click pour configurer l'état de départ ne marche que si la dimension est impaire



public:
    // constructeur à 1 argument (explicit mentionné)
    explicit AutoCell(QWidget* parent = nullptr);
private slots:
    // Q1
    void synchronizeNumToNumBit(int n);
    void synchronizeNumBitToNum(const QString& s);
    // Q2
    void activateBitDepart(const QModelIndex&); // interface de configuration de l'état de départ
    // Q3
    void Simuler();

};

#endif // AUTOCELL_H
