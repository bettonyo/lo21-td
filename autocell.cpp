#include "autocell.h"
#include "automate.h"
#include "automatemanager.h"
#include "simulateur.h"
#include  <QHeaderView>

AutoCell::AutoCell (QWidget* parent) : QWidget(parent) { /// Etape 0 : relier l'AutoCell à son Widget parent !

    /// Création du main main layout qui est vertical
    mainLayout = new QVBoxLayout;

    /// Première colonne :
    num = new QSpinBox;
    num->setRange(0,255);
    num->setValue(0);

    numl = new QLabel("Numéro");

    /// Le premier layout vertical
    numc = new QVBoxLayout;
    numc->addWidget(numl);
    numc->addWidget(num);

    /// Le grand layout horizontal
    numeroc = new QHBoxLayout;
    numeroc->addLayout(numc);

    /// Initialisation du Validator :
    zeroOneValidator = new QIntValidator;
    zeroOneValidator->setRange(0,1);

    /// Intialisation du label des 8 configurations de bits : à la main :
    numeroBitl[0] = new QLabel("111");
    numeroBitl[1] = new QLabel("110");
    numeroBitl[2] = new QLabel("101");
    numeroBitl[3] = new QLabel("100");
    numeroBitl[4] = new QLabel("011");
    numeroBitl[5] = new QLabel("010");
    numeroBitl[6] = new QLabel("001");
    numeroBitl[7] = new QLabel("000");


    /// Créations des Vlayouts représentant les labels et linedit pour les configs de bits :
    for (unsigned int i = 0; i < 8; i++) {
         bitc[i] = new QVBoxLayout;
         //numeroBitl[i] = new QLabel(""); // non, on les crée à la main : il s'agit des 8 configurations de bits
         numeroBit[i] = new QLineEdit;
         numeroBit[i]->setFixedWidth(20);
         numeroBit[i]->setMaxLength(1);
         numeroBit[i]->setText("0");
         numeroBit[i]->setValidator(zeroOneValidator);
         /// A MODIF CAR ON PEUT ECRIRE UN NOMBRE > à 1
         bitc[i]->addWidget(numeroBitl[i]);
         bitc[i]->addWidget(numeroBit[i]);
         // ajout du Vlayout courant dans le main Hlayout
         numeroc->addLayout(bitc[i]);
    }

    // Q2

    /// Intialisation de la Table de départ :

    depart = new QTableWidget(1,dimension);
    depart->horizontalHeader()->setVisible(false);
    depart->verticalHeader()->setVisible(false);
    depart->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    depart->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    depart->setFixedHeight(taille);
    depart->horizontalHeader()->setMinimumSectionSize(taille);

    // création des cellules :
    for (unsigned int i = 0; i < dimension; i++) {
        depart->setColumnWidth(i, taille);
        QTableWidgetItem* cell = new QTableWidgetItem;
        cell->setBackground(Qt::white);
        cell->setForeground(Qt::white);
        // allocation et initialisation de la ième cellule
        depart->setItem(0, i, cell);
        // Pour désactiver complétement l'édition sur la cellule en question :
        cell->setFlags(Qt::ItemIsEditable);

    }

    /// Bouton de lancement de la simulation :
    // Q3
   simulation = new QPushButton("Simulation");

    /// TableWidget pour représenter les états générés :
    // Q3

    etats = new QTableWidget(dimension, dimension);
    etats->horizontalHeader()->setVisible(false);
    etats->verticalHeader()->setVisible(false);
    etats->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    etats->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    etats->setFixedHeight(taille*dimension);
    etats->horizontalHeader()->setMinimumSectionSize(taille);
    etats->verticalHeader()->setMinimumSectionSize(taille);

    // création des cellules
    for(unsigned int i=0; i<dimension; i++) {

        etats->setColumnWidth(i,taille);
        etats->setRowHeight(i,taille);

        for(unsigned int j=0; j<dimension; j++){
            QTableWidgetItem* cell = new QTableWidgetItem;
            cell->setBackground(Qt::white);
            cell->setForeground(Qt::white);
            etats->setItem(j, i, cell);
            cell->setFlags(Qt::ItemIsEditable);
        }

    }


    /// On affecte le mainLayout de notre AutoCell (AutoCell hérite de la classe QWidget et peut donc être considéré comme une fenetre !)
    /// : fenetre.setLayout()

    mainLayout->addLayout(numeroc);
    mainLayout->addWidget(depart);
    mainLayout->addWidget(simulation);
    mainLayout->addWidget(etats);

    this->setLayout(mainLayout);
    this->setFixedWidth(taille*dimension + taille); // on fixe la dimension de la fenetre


    /// Connexions signals et slots

    // Q1
    // changement du num ==> appel méthode synchronizeNumToNumBit(int i)
    connect(num, SIGNAL(valueChanged(int)), this, SLOT(synchronizeNumToNumBit(int)));

    // changement d'un NumBit ==> appel méthode synchronizeNumBitToNum(const QString&)
    for (unsigned int i = 0; i < 8; i++) {
        connect(numeroBit[i], SIGNAL(textChanged(const QString&)), this, SLOT(synchronizeNumBitToNum(const QString&)));
    }

    // Q2
    // il faut connecter chaque cellule à un événement click ! un connec pour chaque cellule
    for (unsigned int i = 0; i < dimension; i++) {
        connect(depart, SIGNAL(clicked(const QModelIndex&)), this, SLOT(activateBitDepart(const QModelIndex&)));
    }

    // Q3
    // bouton de lancement de la simulation
    connect(simulation, SIGNAL(clicked()), this, SLOT(Simuler()));

}

void AutoCell::synchronizeNumToNumBit(int n) {
    std::string s = NumToNumBit(n);
    for (unsigned int i = 0; i < 8; i++) {
        numeroBit[i]->setText(QString(s[i]));
    }
}


void AutoCell::synchronizeNumBitToNum(const QString& s) {
    if (s == "")
        return;

    std::string str;

    for (unsigned int i = 0; i < 8; i++) {
        if (numeroBit[i]->hasAcceptableInput()) { // vérification que le contenu est valide
            str += std::string(numeroBit[i]->text().toLatin1());
        }
        else { // on veut remettre la case à 0 puis quitter
            numeroBit[i]->setText("0");
            return;
        }
    }

    int n = NumBitToNum(str);
    num->setValue(n);

}

void AutoCell::activateBitDepart(const QModelIndex& i) {
    QTableWidgetItem* cell = depart->item(0, i.column()); // renvoie l'indice de la colonne clickée

    if (cell->text() == "") {
        cell->setForeground(Qt::black);
        cell->setBackground(Qt::black);
        cell->setText("1"); // convention choisie pour discriminer les cases noires : on utilise cette convention dans le test
        // il est plus commun de tester une valeur plutot qu'une couleur d'affichage
    }

    else {
        cell->setForeground(Qt::white);
        cell->setBackground(Qt::white);
        cell->setText("");
    }

}

void AutoCell::Simuler() {

    Etat e(dimension);
    // on met configure l'état de départ en fonction de la couleur des cellules
    for(unsigned int i=0; i<dimension; i++){
        if (depart->item(0,i)->text() == "1")
            e.setCellule(i,true);
    }

    const Automate& A = AutomateManager::donneAutomateManager().getAutomate(num->value());
    Simulateur S(A,e);

    for(unsigned int i=0; i<dimension; i++) {
         S.next();
         const Etat& d=S.dernier();

         for(unsigned int j=0; j<dimension; j++) {

            if (d.getCellule(j)) {
                etats->item(i,j)->setBackground(Qt::black);
                etats->item(i,j)->setForeground(Qt::black);
                etats->item(i,j)->setText("1");
            }
            else {
                etats->item(i,j)->setForeground(Qt::white);
                etats->item(i,j)->setBackground(Qt::white);
                etats->item(i,j)->setText("");
            }
         }

     }

}


// Q3 : utiliser les simulations faites dans les TD précédents :
